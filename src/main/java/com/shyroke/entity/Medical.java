package com.shyroke.entity;

import java.util.Date;

public class Medical {
    private Integer id;

    private String name;

    private String chengfen;

    private String yongfa;

    private String zuoyong;

    private String fuzuoyong;

    private String price;

    private String baozhiqi;

    private Date shenchanriqi;

    private String company;

    private String jixing;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getChengfen() {
        return chengfen;
    }

    public void setChengfen(String chengfen) {
        this.chengfen = chengfen;
    }

    public String getYongfa() {
        return yongfa;
    }

    public void setYongfa(String yongfa) {
        this.yongfa = yongfa;
    }

    public String getZuoyong() {
        return zuoyong;
    }

    public void setZuoyong(String zuoyong) {
        this.zuoyong = zuoyong;
    }

    public String getFuzuoyong() {
        return fuzuoyong;
    }

    public void setFuzuoyong(String fuzuoyong) {
        this.fuzuoyong = fuzuoyong;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getBaozhiqi() {
        return baozhiqi;
    }

    public void setBaozhiqi(String baozhiqi) {
        this.baozhiqi = baozhiqi;
    }

    public Date getShenchanriqi() {
        return shenchanriqi;
    }

    public void setShenchanriqi(Date shenchanriqi) {
        this.shenchanriqi = shenchanriqi;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getJixing() {
        return jixing;
    }

    public void setJixing(String jixing) {
        this.jixing = jixing;
    }
}